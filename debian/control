Source: python-qtawesome
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Ghislain Antony Vaillant <ghisvail@gmail.com>,
           Julian Gilbey <jdg@debian.org>
Section: python
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-sequence-python3,
               fonts-fork-awesome,
               python3-all,
               python3-doc <!nodoc>,
               python3-fonttools,
               python3-pyqt6 <!nocheck>,
               python3-pyside6.qtcore <!nocheck>,
               python3-pyside6.qtgui <!nocheck>,
               python3-pyside6.qtopengl <!nocheck>,
               python3-pyside6.qtwidgets <!nocheck>,
               python3-qtpy,
               python3-setuptools,
               python3-sphinx <!nodoc>,
               python3-sphinx-rtd-theme <!nodoc>
Standards-Version: 4.7.0
Rules-Requires-Root: no
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-qtawesome
Vcs-Git: https://salsa.debian.org/python-team/packages/python-qtawesome.git
Homepage: https://github.com/spyder-ide/qtawesome

Package: python3-qtawesome
Architecture: all
Depends: python-qtawesome-common (= ${binary:Version}),
         ${misc:Depends},
         ${python3:Depends}
Suggests: python-qtawesome-doc
Description: iconic fonts in PyQt and PySide applications (Python 3)
 QtAwesome enables iconic fonts such as Font Awesome and Elusive Icons in
 PyQt and PySide applications.
 .
 This package provides QtAwesome for Python 3.  Note that this package
 does not depend on any Python Qt toolkit but instead uses qtpy; the
 depending application needs to depend on an appropriate toolkit (and
 set QT_API if needed; see the python3-qtpy documentation); these are
 python3-pyqt5 for PyQt5, python3-pyqt6 for PyQt6 and
 python3-pyside6.qtcore, python3-pyside6.qtgui,
 python3-pyside6.qtopengl, python3-pyside6.qtwidgets for PySide 6.

Package: python-qtawesome-common
Architecture: all
Multi-Arch: foreign
Depends: fonts-elusive-icons,
         fonts-font-awesome,
         ${misc:Depends}
Description: common files for QtAwesome
 QtAwesome enables iconic fonts such as Font Awesome and Elusive Icons in
 PyQt and PySide applications.
 .
 This package provides the files common to all QtAwesome Python packages.
 Font Awesome 5 is replaced with Fork Awesome which might have a visual
 effect in packages that make use of Font Awesome 5.

Package: python-qtawesome-doc
Architecture: all
Multi-Arch: foreign
Section: doc
Depends: python3-doc,
         ${misc:Depends},
         ${sphinxdoc:Depends}
Recommends: python3-qtawesome (= ${binary:Version})
Built-Using: ${sphinxdoc:Built-Using}
Description: documentation and examples for QtAwesome
 QtAwesome enables iconic fonts such as Font Awesome and Elusive Icons in
 PyQt and PySide applications.
 .
 This package provides the documentation and examples for QtAwesome.
Build-Profiles: <!nodoc>
